<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="edit.css"> 
    <title>Form Edit Data</title>
</head>
<body>
<?php
include ('koneksi.php');
//MENGAMBIL DATA PADA URL DENGAN KEY ID
$id = $_GET['id'];

//MENGAMBIL DATA BERDASARKAN ID
$query = mysqli_query($connect, "SELECT * FROM karyawan WHERE id = $id") or die (mysqli_error($connect));
$data = mysqli_fetch_array($query);

$role = $data['roles'];
$status = $data['status']

?>
    <form action="prosesEdit.php?id=<?php echo $data['id'] ?>" method="POST">
    <h1>EDIT DATA</h5>
    Username:
    <input type="text" name="username" value="<?php echo $data['username'] ?>"><br>
    Nama:
    <input type="text" name="nama" value="<?php echo $data['nama']?>"><br>
    Roles :
    <select name="roles" >
        <option value="Admin" <?php if($role == 'Admin'){echo "selected";} ?>>Admin</option>
        <option value="Kasir" <?php if($role == 'Kasir'){echo "selected";} ?>>Kasir</option>
    </select><br>
    <h5>Status :</h5>
        <input name="status" value="aktif" type="radio" id="Aktif_radio"<?php if($status == 'aktif'){echo "checked";} ?>><label for="Aktif_radio">Aktif<br>
        <input name="status" value="Nonaktif" type="radio" id="Nonaktif_radio"<?php if($status == 'Nonaktif'){echo "checked";}?>><label for="Nonaktif_radio">Nonaktif<br>
    <input type="submit" value="Simpan"> <button><a href="index.php">Batal</a>
 </form>
 
</body>
</html>